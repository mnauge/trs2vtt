# trs2vtt

Convertir un fichier .TRS (transcriber) vers un fichier de sous titrage pour le web .VTT (https://developer.mozilla.org/fr/docs/Web/API/WebVTT_API)

*Michael Nauge, Université de Poitiers*

Quelques lignes de codes pour convertir un fichier de transcription .TRS (Transcriber) vers un fichier de sous titrage pour le web .VTT

## Trs
Voici une portion de fichier .TRS

``` xml
<Turn startTime="0" endTime="4.746" speaker="spk1" mode="spontaneous">
<Sync time="0"/>
C'était une jeune fille de quinze ans
</Turn>
<Turn speaker="spk1" mode="spontaneous" startTime="4.746" endTime="10.293">
<Sync time="4.746"/>
Et quelques jours en avantage
</Turn>
<Turn speaker="spk1" mode="spontaneous" startTime="10.293" endTime="16.453">
<Sync time="10.293"/>
Son père la fait mettre à la tour
</Turn>
```

# Vtt
Voici une portion de ficher .VTT (attendu après conversion)
``` xml
WEBVTT


00:00:00.000 --> 00:00:04.000
C'était une jeune fille de quinze ans

00:00:04.000 --> 00:00:10.000
Et quelques jours en avantage

00:00:10.000 --> 00:00:16.000
Son père la fait mettre à la tour
```

[visualiser le code avec nbviewer](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/mnauge/trs2vtt/-/raw/master/trs2vtt.ipynb?flush_cache=true)
[executer/modifier le code avec myBinder](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmnauge%2Ftrs2vtt/c625e15e986c57252baae11cba70b9aa7a9acb5f?filepath=trs2vtt.ipynb)